package Translator;

import java.util.ArrayList;

public class Translator {
    private final Dictionary dictionary;
    private final String[] punctuationArray = {",", ".", "?", "!", ":", ";"};

    public Translator(Dictionary dict) {
        this.dictionary = dict;
    }

    public String translate(String text) {
        text = text.toLowerCase();

        StringBuilder translatedText = new StringBuilder();

        String[] words = text.split(" ");

        String translation = null;
        String punctuation = null;
        String originalPunctuation = null;

        int index = 0;

        for (int i = 0; i < words.length; ++i) {
            index = 0;

            ArrayList<String> translations = new ArrayList<>();
            StringBuilder phrase = new StringBuilder();
            StringBuilder originalPhrase = new StringBuilder();

            phrase.append(words[i]);
            originalPhrase.append(words[i]);

            originalPunctuation = isContainsPunctuation(originalPhrase.toString());
            if (originalPunctuation != null) {
                originalPhrase.deleteCharAt(originalPhrase.length() - 1);
            }

            translation = dictionary.getTranslation(originalPhrase.toString());
            if (translation != null) {
                translations.add(translation);
            }

            for (int j = i + 1; j < words.length; ++j) {
                phrase.append(" ");
                StringBuilder word = new StringBuilder(words[j]);
                punctuation = isContainsPunctuation(word.toString());
                if (punctuation != null) {
                    word.deleteCharAt(word.length() - 1);
                }
                phrase.append(word);

                translation = dictionary.getTranslation(phrase.toString());
                if (translation != null) {
                    if (punctuation != null) {
                        translation = translation.concat(punctuation);
                    }
                    translations.add(translation);
                    index = j - i;
                }
            }

            if (!translations.isEmpty()) {
                if (originalPunctuation != null) {
                    translatedText.append(translations.getLast().concat(originalPunctuation));
                } else {
                    translatedText.append(translations.getLast());
                }
                translatedText.append(" ");
                i += index;
            } else {
                if (originalPunctuation != null) {
                    translatedText.append(originalPhrase.append(originalPunctuation));
                } else {
                    translatedText.append(originalPhrase);
                }
                translatedText.append(" ");
            }
        }
        return translatedText.toString();
    }

    private String isContainsPunctuation(String word) {
        for (String ch : punctuationArray) {
            if (word.contains(ch)) {
                return ch;
            }
        }
        return null;
    }
}
