package Translator;

import exceptions.InvalidFileFormatException;
import exceptions.FileReadException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class Dictionary {
    private final Map<String, String> dict = new HashMap<>();

    public Dictionary(File file) throws InvalidFileFormatException, FileReadException {
        readDictionary(file);
    }

    private void readDictionary(File dictionary) throws InvalidFileFormatException, FileReadException {
        try (Scanner scanner = new Scanner(dictionary)) {
            boolean validDictionary = true;
            while (scanner.hasNextLine()) {
                String translation = scanner.nextLine();
                translation = translation.toLowerCase();
                String[] translationPair = translation.split("\\|");
                if (translationPair.length != 2) {
                    validDictionary = false;
                    break;
                }
                dict.put(translationPair[0].trim(), translationPair[1].trim()); // trim removes spaces

            }
            if (dict.isEmpty() || !validDictionary) {
                throw new InvalidFileFormatException();
            }
        } catch (FileNotFoundException e) {
            throw new FileReadException();
        }
    }

    public String getTranslation(String phrase) {
        if (dict.containsKey(phrase.toLowerCase()))
        {
            return dict.get(phrase.toLowerCase());
        }
        return null;
    }
}
