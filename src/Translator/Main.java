package Translator;

import exceptions.FileReadException;
import exceptions.InvalidFileFormatException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        File file = new File("dict.txt");
        try {
            Dictionary dictionary = new Dictionary(file);
            Translator translator = new Translator(dictionary);
            String translated = translator.translate(readFile("text.txt", StandardCharsets.UTF_8));
            System.out.println(translated);
        }
        catch (InvalidFileFormatException | FileReadException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private static String readFile(String path, Charset encoding) throws FileReadException {
        try {
            return Files.readString(Paths.get(path), encoding);
        } catch (IOException e) {
            throw new FileReadException();
        }
    }
}